# CloudFinder

Python 3 program to detect clouds in images using OpenCV



## Running the script
```
python find-cloud.py <folder>
```
Where `<folder>` is the name of the directory where the input images are located.

Use this command to run `find-cloud.py` for all images in the `cloudpix` folder:
```
python find-cloud.py cloudpix
```


## Restrictions
The program only accepts images in .jpg format. It generates an error message if there are no such files in the specified directory.

The program doesn't produce accurate results for images with poor lighting - a cloudy night-time sky will be mostly blue so it will be confused with being clear.

Cirrus clouds also pose problems - since they are thinner than other types, their colour doesn't differ too much from the surrounding sky, so the program will not mark them in yellow as expected.

Other issues may appear if something is partially obstructing the view of the sky, such as the spider in image 0612173.jpg, as the program will mistakenly mark it in yellow. This problem is partially avoided for the tree and the building on the left.


## Images
All images in the `cloudpix` folder were provided by [Dr Adrian Clark] (https://www.essex.ac.uk/people/clark65804/adrian-clark).