#---------------------------------------------------------------------------
# find-cloud.py
#---------------------------------------------------------------------------
#
# PURPOSE
# This script processes a series of images by marking the pixels containing
# clouds in yellow.
#
# USAGE
# python find-cloud.py <folder>
# Where <folder> is the name of the directory where the input images are
# located.
#
# RESTRICTIONS
# The program only accepts images in .jpg format. It generates an error message
# if there are no such files in the specified directory.
#
# The program doesn't produce accurate results for images with poor lighting, 
# - a cloudy night-time sky will be mostly blue so it will be confused with being
# clear.
#
# Cirrus clouds also pose problems - since they are thinner than other types of
# clouds, their colour doesn't differ too much from the surrounding sky, so the
# program will not mark them in yellow as expected.
#
# Other issues may appear if there is something partially obstructing the view
# of the sky, such as the spider in image 0612173.jpg, as the program will
# mistakenly mark it in yellow. This problem is partially avoided for the tree
# and the building on the left.
#

import sys, cv2, numpy, os, pylab

#---------------------------------------------------------------------------
# Function to process an image
#---------------------------------------------------------------------------
def findClouds(img):
  img = img.copy()
  
  # Remove noise
  img = cv2.fastNlMeansDenoisingColored(img, None, 10, 10, 7, 21)

  # Convert to HSV
  hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

  # Define colour ranges in HSV
  LOWER_SKY = numpy.array([75, 40, 75], numpy.uint8)
  UPPER_SKY = numpy.array([115, 255, 255], numpy.uint8)
  
  LOWER_BUILDING = numpy.array([0, 0, 0], numpy.uint8)
  UPPER_BUILDING = numpy.array([180, 255, 90], numpy.uint8)
  
  LOWER_TREE = numpy.array([0, 0, 0], numpy.uint8)
  UPPER_TREE = numpy.array([130, 255, 75], numpy.uint8)

  # Mask all areas that are not sky
  mask_sky = cv2.inRange(hsv, LOWER_SKY, UPPER_SKY)
  
  # Mask tree and building - they are roughly in the same place
  mask_building = cv2.inRange(hsv[:190, :53], LOWER_BUILDING, UPPER_BUILDING)
  mask_tree = cv2.inRange(hsv[475:, :176], LOWER_TREE, UPPER_TREE)
  
  # Mask the original image using bitwise and
  res = cv2.bitwise_and(img, img, mask = mask_sky)
 
  # Mark the tree and building in red
  res[:190, :53][numpy.where(mask_building != [0])] = [0,0,255]
  res[475:, :176][numpy.where(mask_tree != [0])] = [0,0,255]
  
  # Any remaining black pixels must be clouds so make them yellow
  img[numpy.where((res == [0,0,0]).all(axis = 2))] = [0,255,255]
  
  return img


#---------------------------------------------------------------------------
# Main program
#---------------------------------------------------------------------------

if len(sys.argv) == 2:
  images = []

  if os.path.isdir(sys.argv[1]) == False:
    print("Directory", sys.argv[1], "does not exist", file=sys.stderr)
    sys.exit(1)

  # Add image paths to the list
  for path in os.listdir(sys.argv[1]):
    if ".jpg" in path:
      images.append(sys.argv[1] + "\\" + path)

  if len(images) == 0:
    print("The directory doesn't contain any .jpg files", file=sys.stderr)
    sys.exit(1)
    

else:
  print("Usage: \n", sys.argv[0], "<folder>", file=sys.stderr)
  sys.exit(1)



# Process the images
for filename in images:
  imgOrig = cv2.imread(filename)
  if imgOrig is not None:

    imgProc = findClouds(imgOrig)

    # Show each original image next to its highlighted version for 2 seconds
    both = numpy.hstack((imgOrig, imgProc))
    cv2.imshow(filename, both)
    cv2.waitKey(2000)
    cv2.destroyWindow(filename)



